import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native';
import withVoice from '../utilities/withVoice';

class OtherScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Other Screen</Text>
        <Text style={styles.instructions}>Press the button and start speaking.</Text>
        <Text style={styles.stat}>{`Started: ${this.props.started}`}</Text>
        <Text style={styles.stat}>{`Recognized: ${this.props.recognized}`}</Text>
        <Text style={styles.stat}>{`Pitch: ${this.props.pitch}`}</Text>
        <Text style={styles.stat}>{`Error: ${this.props.error}`}</Text>
        <Text style={styles.stat}>Results</Text>
        {this.props.results.map((result, index) => {
          return (
            <Text key={`result-${index}`} style={styles.stat}>
              {result}
            </Text>
          );
        })}

        <Text style={styles.stat}>Partial Results</Text>
        {this.props.partialResults.map((result, index) => {
          return (
            <Text key={`partial-result-${index}`} style={styles.stat}>
              {result}
            </Text>
          );
        })}
        <Text style={styles.stat}>{`End: ${this.props.end}`}</Text>
        <TouchableHighlight onPress={this.props._startRecognizing}>
          <Image style={styles.button} source={require('../screens/button.png')} />
        </TouchableHighlight>
        <TouchableHighlight onPress={this.props._stopRecognizing}>
          <Text style={styles.action}>Stop Recognizing</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={this.props._cancelRecognizing}>
          <Text style={styles.action}>Cancel</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={this.props._destroyRecognizer}>
          <Text style={styles.action}>Destroy</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

export default withVoice(OtherScreen);

const styles = StyleSheet.create({
  button: {
    width: 50,
    height: 50,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  action: {
    textAlign: 'center',
    color: '#0000FF',
    marginVertical: 5,
    fontWeight: 'bold',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  stat: {
    textAlign: 'center',
    color: '#B0171F',
    marginBottom: 1,
  },
});
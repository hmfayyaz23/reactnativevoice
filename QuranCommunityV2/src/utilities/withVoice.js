import React from 'react';
import Voice from 'react-native-voice';
import { Platform } from 'react-native';

let timer = null;

const withVoice = (WrappedComponent) => {
  class HOC extends React.Component {

    state = {
      recognized: '',
      pitch: '',
      error: '',
      end: '',
      started: '',
      results: [],
      partialResults: [],

      listening: false,
    };

    constructor(props) {
      super(props);
      Voice.onSpeechStart = this.onSpeechStart;
      Voice.onSpeechRecognized = this.onSpeechRecognized;
      Voice.onSpeechEnd = this.onSpeechEnd;
      Voice.onSpeechError = this.onSpeechError;
      Voice.onSpeechResults = this.onSpeechResults;
      Voice.onSpeechPartialResults = this.onSpeechPartialResults;
      Voice.onSpeechVolumeChanged = this.onSpeechVolumeChanged;
    }
  
    // Lifecycle
    componentWillUnmount() {
      Voice.destroy().then(Voice.removeAllListeners);
    }

    shouldComponentUpdate(nextState){
      if (this.state.listening !== nextState.listening)
      {
        return true;
      }
      return false;
    }

    // Event Handlers
    onSpeechStart = e => {
      // eslint-disable-next-line
      console.log('onSpeechStart: ', e);
      this.setState({
        started: '√',
      });
    };

    onSpeechRecognized = e => {
      // eslint-disable-next-line
      console.log('onSpeechRecognized: ', e);
      this.setState({
        recognized: '√',
      });
    };

    onSpeechEnd = e => {
      if (Platform.OS==='ios')
      {
        timer=null;
        this.setState({ listening:false});
        if (this.state.results != null && this.state.results != '')
        {
          console.log('onSpeechEnd: ', e);
          this.setState({
            end: '√',
          });
          // do what you want with the result (for iOS)
        }
       }else{
        console.log('onSpeechEnd: ', e);
        this.setState({
          end: '√',
        });
       }
    };
  
    onSpeechError = e => {
      // eslint-disable-next-line
      console.log('onSpeechError: ', e);
      this.setState({
        error: JSON.stringify(e.error),
      });
    };

    onSpeechResults = e => {
      if (Platform.OS==='ios')
      {
        this.setState({results : e.value[0]});
        if (timer!==null)
        {
            clearTimeout(timer);
        }
        timer=setTimeout(()=>{
          this._stopRecognizing();
        },2000);
      }
      else
      {
        console.log('onSpeechResults: ', e);
        this.setState({
          results: e.value, 
          listening: false
        });
        // Do what you want with the result (for android)
      }
    };
  
    onSpeechPartialResults = e => {
      // eslint-disable-next-line
      console.log('onSpeechPartialResults: ', e);
      let speech = e.value[0].toLowerCase().split(" ").slice(-1)[0];
  
      if (speech.includes("quran")) {
        this.props.navigation.navigate('Quran', {});
        this.setState({
          partialResults: e.value,
        });
      } else if(speech.includes("back")) {
        this.props.navigation.navigate('Home', {});
        this.setState({
          partialResults: e.value,
        });
      }else{
        this.setState({
          partialResults: e.value,
        });
      }
    };

    onSpeechVolumeChanged = e => {
      // eslint-disable-next-line
      console.log('onSpeechVolumeChanged: ', e);
      this.setState({
        pitch: e.value,
      });
    };

    // Voice
    _startRecognizing = async () => {
    this.setState({
      recognized: '',
      pitch: '',
      error: '',
      started: '',
      results: [],
      partialResults: [],
      end: '',
    });

    try {
      await Voice.start('en-US');
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
    this.setState({ listening:true});
  };

  _stopRecognizing = async () => {
    try {
      await Voice.stop();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
  };

  _cancelRecognizing = async () => {
    try {
      await Voice.cancel();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
  };

  _destroyRecognizer = async () => {
    try {
      await Voice.destroy();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
    this.setState({
      recognized: '',
      pitch: '',
      error: '',
      started: '',
      results: [],
      partialResults: [],
      end: '',
    });
  };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          started = {this.state.started}
          recognized = {this.state.recognized}
          pitch = {this.state.pitch}
          error = {this.state.error}
          results = {this.state.results}
          partialResults = {this.state.partialResults}
          end = {this.state.end}

          _startRecognizing={this._startRecognizing}
          _stopRecognizing={this._stopRecognizing}
          _cancelRecognizing={this._cancelRecognizing}
          _destroyRecognizer={this._destroyRecognizer}

        />
      );
    }
  }

  return HOC;
}

export default withVoice;
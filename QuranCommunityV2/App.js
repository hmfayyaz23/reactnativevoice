import React from 'react';
//first import createStackNavigator from react-navigation
//then import StackNavigator for creatign nested routes
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
//Import your screens 
//import VoiceTest from './src/utilities/VoiceTest';
import HomeScreen from './src/screens/Home';
import OtherScreen from './src/screens/Other';


//Define your routes using createStackNavigator, which will be a object full of options. 
const MainNavigator = createStackNavigator({
    //Define your screens.
    Home: { screen: HomeScreen },
    Quran: { screen: OtherScreen },
  },
  {
    initialRouteName:  'Home'
  })


//Export default the stateless component 
const App = createAppContainer(MainNavigator);

export default App;